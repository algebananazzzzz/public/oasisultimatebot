variable "aws_region" {
  default = "ap-southeast-1"
}

variable "env" {
  type    = string
  default = "prd"
}

variable "project_code" {
  default = "oasisultimatebot"
}
