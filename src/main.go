package main

import (
	"context"
	"encoding/json"
	"net/http"
	"os"

	"log"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

var bot *tgbotapi.BotAPI

func init() {
	var err error
	bot, err = tgbotapi.NewBotAPI(os.Getenv("TOKEN"))
	if err != nil {
		log.Panic(err)
	}

	log.Printf("Initialized cold start of bot: %s", bot.Self.UserName)
}

func Handler(ctx context.Context, req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	var update tgbotapi.Update

	if err := json.Unmarshal([]byte(req.Body), &update); err != nil {
		log.Panic(err)
		return events.APIGatewayProxyResponse{StatusCode: http.StatusBadRequest}, nil
	}

	if err := processUpdate(update); err != nil {
		bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "🙊 Oops! It seems like our frisbee got stuck in the tree this time. 🌳🥏 Please explore other functions of the bot while we look into it! 🌟"))
		log.Panic(err)
		return events.APIGatewayProxyResponse{StatusCode: http.StatusBadRequest}, nil
	}

	return events.APIGatewayProxyResponse{StatusCode: http.StatusOK}, nil
}

func main() {
	lambda.Start(Handler)
}
