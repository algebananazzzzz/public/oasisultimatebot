package main

const (
	helpMessage = "👉 Type '/help' for a guide on how to navigate this bot! 🌈🥏😄"

	unknownCommandMessage = "🧱 Brick! Seems like your command is like a pull out of bounds. We can keep our throws in play by referring to '/help'! 🚀🌈"
)
