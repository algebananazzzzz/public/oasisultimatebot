package main

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

func defaultHandler(update tgbotapi.Update) {
	var message string
	switch update.Message.Command() {
	case "start":
		message = "🌟 <b>Welcome to the Oasis Ultimate!</b> 🌟\n" +
			"\n" +
			"Hey there! We're thrilled to have you in our frisbee-loving family. 🥏 Whether you're a seasoned Ultimate player or new to the game, Ultimate Oasis is a place where you can grow, build connections, and make a difference! Let's dive into the world of high-flying, disc-tossing adventures together! 🚀\n" +
			"\n" + helpMessage
	case "aboutus":
		message = `🥏 <b>About Oasis Ultimate</b> 🥏
    		
<b>Our Vision</b>
🌟 A club built upon SOTG & Christian values to bring like-minded ultimate players together while providing a mid-tier environment for anyone looking for a family-focused community/club.
		
<b>Our Mission</b>
✨ 1. Individual Growth
- <b>Body</b>: To become a better athlete
- <b>Mind</b>: To become stronger mentally (resilience, discipline)
- <b>Soul</b>: To become a better person (character development, SOTG)

💖 2. Family Focused
- To provide a <b>safe and forgiving</b> environment to enjoy Ultimate with your whole heart.
- Learn to <b>resolve conflicts</b> amicably, both on and off the field.
- To be <b>more than just teammates</b>; we're a family that supports each other!
		
🤝 3. Impacting the Community
- We provide a <b>safe space</b> for interested individuals to learn a new sport.
- We believe in <b>giving back to the community</b>, especially among youths at risk and our friends and family.
		
<b>Our Values</b>
🌟 Discipline, Spirit, Forgiveness, Humility, Integrity, Joy, Love and Empathy
		
Explore our club's inner workings and how we bring our vision and mission to life with '/ourmodel'! 🌈🥏💪`
	case "ourmodel":
		message = `🚀 <b>Our Model</b> 🚀

<b>Oasis Ultimate</b> is built on three key arms:

<b>1. Competitive Arm 🏆</b>
- This arm dedicates on the players and team’s overall competitive development
- This is done through rigorous training and strategic gameplay.

<b>2. Developmental Arm 🌱</b>
- We provide safe and inclusive spaces for newer players to thrive. 
- This arm of our club encourages learning, mentorship, and a sense of belonging.

<b>3. Social Arm 🌟</b>
- Our social arm reaches out to youths-at-risk, friends, and families interested in learning a new sport.
- Current players give back to the community by learning to coach and mentor others.

🔄 <b>Positive Cycle of Growth and Giving Back</b> 🔄
- Players start their Ultimate journey within our Social Arm through outreach. 
- As they grow, they progress toward the Competitive Arm by enhancing their skills and teamwork.
- This journey comes full circle as they give back by becoming mentors and coaches within the Social Arm.

By harmonizing these three arms, we not only excel on the field but also build strong character and a close-knit community. Together, we drive positive change within the wider community. 🥏🌈💪`
	case "help":
		message = `Here are some helpful commands to explore our club:

- <b>/aboutus</b>: Learn about our Vision, Mission, and Values.
- <b>/ourmodel</b>: Discover how we achieve our Vision and Mission.
- <b>/ourroles</b>: Get an Overview of the Roles within our Club.
- <b>/joinus</b>: Find out how to become a part of our Ultimate family.
- <b>/contactus</b>: Reach out if you have questions or need assistance.

Use these commands to navigate and learn more about <b>Oasis Ultimate</b>!`
	case "ourroles":
		message = `<b>Our Roles</b> 🏢

Here's a breakdown of the roles within Oasis:
		
<b>Training</b>:
- Responsible for planning training sessions.
- Conducts demonstrations and guides new players through drills.
- Offers leadership coaching.
		
<b>Spirit/Welfare</b>:
- Ensures a positive spirit both on and off the field.
- Facilitates discussions amongst player pods.
- Holds players accountable to their goals.
- Plans our internal gatherings and events.
		
<b>Events/Communications</b>:
- Manages our Instagram account.
- Liaises with external stakeholders, such as SAMH and other social service agencies, for Oasis Gives Back.
- Plans our external events.`
	case "":
		message = helpMessage
	default:
		message = unknownCommandMessage
	}
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, message)
	msg.ParseMode = tgbotapi.ModeHTML
	bot.Send(msg)
}

func processUpdate(update tgbotapi.Update) error {
	defaultHandler(update)
	return nil
}
