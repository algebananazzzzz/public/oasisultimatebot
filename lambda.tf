locals {
  function_name       = "${var.env}-app-func-${var.project_code}"
  execution_role_name = "${var.env}-app-role-${var.project_code}"
  execution_role_policy_document = {
    name = "${var.env}-app-policy-${var.project_code}"
    statements = {
      allowCreateNetworkInterface = {
        effect = "Allow"
        actions = [
          "ec2:CreateNetworkInterface",
          "ec2:DescribeNetworkInterfaces",
          "ec2:DeleteNetworkInterface",
          "ec2:AssignPrivateIpAddresses",
          "ec2:UnassignPrivateIpAddresses"
        ]
        resources = ["*"]
      }
    }
  }
}

data "aws_ssm_parameter" "token" {
  name = "/${var.env}/app/${var.project_code}/TOKEN"
}

module "lambda_function" {
  source        = "github.com/algebananazzzzz/terraform_modules/modules/lambda_function"
  function_name = local.function_name
  runtime       = "provided.al2"
  handler       = "bootstrap"
  environment_variables = {
    TOKEN = data.aws_ssm_parameter.token.value
  }
  execution_role_name            = local.execution_role_name
  execution_role_policy_document = local.execution_role_policy_document
  deployment_package = {
    local_path = "./build"
  }

  vpc_config = {
    subnet_ids         = data.aws_subnets.private.ids
    security_group_ids = [data.aws_security_group.allow_nat.id]
  }
}


