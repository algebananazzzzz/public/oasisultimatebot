module oasisultimatebot/src

go 1.21.2

require (
	github.com/aws/aws-lambda-go v1.41.0
	github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.5.1
)
